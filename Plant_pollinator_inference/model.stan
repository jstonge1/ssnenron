// code from jg Younf
// Mixture model

data {
  // Dimensions of the data matrix, and matrix itself.
  int<lower=1> n_p;
  int<lower=1> n_a;
  int<lower=0> M[n_p, n_a];
}
transformed data {
  // Pre-compute the marginals of M to save computation in the model loop.
  int M_rows[n_p] = rep_array(0, n_p);
  int M_cols[n_a] = rep_array(0, n_a);
  int M_tot = 0;
  for (i in 1:n_p) {
    for (j in 1:n_a) {
      M_rows[i] += M[i, j];
      M_cols[j] += M[i, j];
      M_tot += M[i, j];
    }
  }
}
parameters {
  real<lower=0> C;  // effect of the overall time or land area of observation by an overall constant C that multiplies the mean μ_ij 
  real<lower=0> r;  // represent the larger number of visits when B_ij = 1, versus when it is 0.
  simplex[n_p] sigma;  // abundances of topic i 
  simplex[n_a] tau;    // abundances of community member i
  real<lower=0, upper=1> rho;  // probability of an edge
}
model {
  // Global sums and parameters
  target += M_tot * log(C) - C;
  // Weighted marginals of the data matrix 
  for (i in 1:n_p) {
    target += M_rows[i] * log(sigma[i]);
  }
  for (j in 1:n_a) {
    target += M_cols[j] * log(tau[j]);
  }
  // Pairwise loop
  for (i in 1:n_p) {
    for (j in 1:n_a) {
      real nu_ij_0 = log(1 - rho);
      real nu_ij_1 = log(rho) + M[i,j] * log(1 + r) - C * r * sigma[i] * tau[j]; // μ_ij = C*σ_i*τ_j (1 + r B_ij )
      if (nu_ij_0 > nu_ij_1)
        target += nu_ij_0 + log1p_exp(nu_ij_1 - nu_ij_0);  // log1p_exp: natural logarithm of one plus the natural exponentiation of x
      else
        target += nu_ij_1 + log1p_exp(nu_ij_0 - nu_ij_1);
    }
  }
} 
generated quantities {
  // Posterior edge probability matrix
  real<lower=0> Q[n_p, n_a];
  for (i in 1:n_p) {
    for (j in 1:n_a) {
      real nu_ij_0 = log(1 - rho);
      real nu_ij_1 = log(rho) + M[i,j] * log(1 + r) - C * r * sigma[i] * tau[j];
      if (nu_ij_1 > 0) 
        Q[i, j] = 1 / (1+ exp(nu_ij_0 - nu_ij_1));
      else
        Q[i, j] = exp(nu_ij_1) / (exp(nu_ij_0) + exp(nu_ij_1));
    }
  }
}