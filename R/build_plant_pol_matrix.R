#
# Script pour générer et visualiser la matrix PP
# idéalement à rouler sur serveur starship pour que les paths fonctionnent
# les paths pourront être réglés une fois le nettoyage faites après le 6
#

library(readr)
library(dplyr)
library(tidyr)
library(RColorBrewer)
library(pheatmap)
library(tidytext)

source("R/prepData2plot.R")
source("R/plotTextSummary.R")

setwd("~/ssnenron/")

# load data
enron_covar <- readRDS("/data/Jonathan/dat_enron/prep_models/enron_metadata4.rds")
enron_sparse <- readRDS("/data/Jonathan/dat_enron/prep_models/enron_sparse4.rds")
topic_model_70 <- readRDS("/data/Jonathan/dat_enron/fit/enron70_byDate_1_31_20.rds")
communities <- read_csv("/data/Jonathan/dat_enron/wdcNSBMexp.csv")

# 70 topics
td_gamma70 <- tidy(topic_model_70, "gamma", document_names = rownames(enron_sparse))
td_beta70 <- tidy(topic_model_70, "beta")

prep_pp_matrix <- function(td_gamma) {
  td_gamma %>% 
    group_by(document) %>% 
    top_n(1, gamma) %>% 
    ungroup() %>% 
    mutate(document = as.numeric(document)) %>% 
    left_join(enron_covar, by = c("document" = "message_id")) %>% 
    left_join(communities, by = "core_sender_name") %>% 
    mutate(date = lubridate::as_datetime(date),
           month = lubridate::floor_date(date, "month")) %>% 
    select(document, topic, c, month) %>%
    #filter(month >= "2001-01-01 00:00:00" & month <= "2001-03-01 00:00:00") %>% 
    count(c, topic, sort = TRUE) %>% 
    spread(
      key = c, 
      value = n, 
      fill = 0
      )
}

calc_p1(topic_model_70, enron_covar)
pp_matrix70 <- prep_pp_matrix(td_gamma70) 

plot_heatmap <- function(pp_mat, list_to_remove) {
  if (is.null(list_to_remove) == TRUE) {
    pp_mat_clean <- pp_mat %>% column_to_rownames("topic")
    colnames(pp_mat_clean) <- paste0("com_", colnames(pp_mat_clean))  
    rownames(pp_mat_clean) <- paste0("topic_", rownames(pp_mat_clean))
    pheatmap::pheatmap(as.matrix(pp_mat_clean), treeheight_row = 0, treeheight_col = 0)
  } else {
  pp_mat_clean <- pp_mat[list_to_remove, ]  %>%  column_to_rownames("topic")
  colnames(pp_mat_clean) <- paste0("com_", colnames(pp_mat_clean))  
  rownames(pp_mat_clean) <- paste0("topic_", rownames(pp_mat_clean))
  coul <- colorRampPalette(brewer.pal(8, "Blues"))(25)
  pheatmap(as.matrix(pp_mat_clean), 
                     treeheight_row = 0, 
                     treeheight_col = 0, color = coul)
  }
}

top_terms <- get_top_terms(tidy(topic_model_70))

library("spacyr")
spacy_initialize()

parsedtxt <- spacy_parse(top_terms$terms) %>% 
  group_by(doc_id) %>% nest() 
top_terms <- top_terms %>% 
  bind_cols(parsedtxt)

top_terms_POS <- top_terms %>% 
  select(-c(terms, doc_id)) %>% 
  unnest(everything()) %>% 
  filter(pos != "PUNCT") %>% 
  group_by(topic, pos) %>% 
  summarise(n = n()) %>% 
  mutate(pct = n / sum(n))

# to examine specific topics
top_terms %>% filter(topic == 18) %>% 
  unnest(c(data)) %>% 
  filter(pos != "PUNCT")

stm::labelTopics(topic_model_70, topics = c(1, 18, 37, 42, 49, 69), n=15)

############################
# RULES FOR PRUNING TOPICS #
############################

# RULE 1: pct_VERB > 0.3 verbs, its junk
# FILTERED: 29 40 58 63
list_topics_mainly_verbs <- top_terms_POS %>% 
  filter(pos == "VERB" & pct > 0.30) %>% pull(topic)

# RULE 2: pct_NOUN < 0.4 & PROPN > 0.4 (see topic 21, 7, 57) & PROPN are not mainly PERSON | DATE (see topic 1, 7)
# FILTERED: 2 (many ADJ), 19, 29,59,  63, 69, 70 (.25 VERB, .4 NOUN), 7 (only months), 37, 40
top_terms_POS %>%
  filter(pos == "NOUN" | pos == "PROPN") %>% 
  select(-n) %>% 
  pivot_wider(names_from = pos, values_from = pct) %>% 
  filter(NOUN < .4)

# RULE 3: ENTITIES >= 5 & (pct_PERSON_B >= 0.5  | pct_DATE_B >= 0.5)
# FILTERED: 18
top_terms %>%
  unnest(c(data)) %>% filter(pos != "PUNCT") %>% 
  group_by(topic, entity) %>% 
  summarise(n = n()) %>% 
  na_if("") %>% 
  pivot_wider(names_from = entity, values_from = n) %>% 
  select(-`NA`) %>% 
  replace(is.na(.), 0) %>% 
  column_to_rownames("topic") %>% 
  mutate(tot_entities = rowSums(.), 
         pct_pers = PERSON_B / tot_entities,
         pct_date = DATE_B / tot_entities) %>% 
  rownames_to_column("topic") %>% 
  as_tibble() %>% 
  select(topic, tot_entities:pct_date) %>% 
  filter(tot_entities > 5 & (pct_pers >= .5 | pct_date >= .5))

#
# SUMMARY
# junk: 63, 29, 37, 40, 
# mostly junk: 70, 59, 
# to verify: 41, 25, 19, 69 (mostly names), 25 (fantasy league),
# looks fine, but ...: 4
#
list_to_remove70 <- -c(54, 69, 55, 36, 32, 21, 41, 37, 39, 42, 
                       1, 29, 34, 59, 63, 70, 2, 40, 18)


plot_heatmap(pp_matrix70, c(list_to_remove70, -c(list_topics_mainly_verbs)))

# pp_matrix[-c(12, 19),]  %>%
#   pivot_longer(-topic, names_to = "community", values_to = "values" ) %>%
#   mutate(topic = reorder_within(topic, values, community),
#                  topic = str_remove_all(topic, "__.+")) %>%
#   ggplot(aes(x=topic, y=values, fill =as.factor(community))) +
#   geom_col(show.legend = FALSE) +
#   facet_wrap(~community) +
#   scale_x_reordered() + 
#   theme_bw()

# readr::write_csv(pp_matrix, "Plant_pollinator_inference/pp_matrix_01.20.20.csv")  
