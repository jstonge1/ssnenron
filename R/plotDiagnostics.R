library(magrittr)
library(dplyr)
library(purrr)
library(stm)
library(tidyr)
library(ggplot2)

# plot model diagnostics
plot_model_diagnostics <- function(k_result, subtitle, write_path) {
  k_result %>%
    transmute(K,
              `Lower bound` = lbound,
              Residuals = map_dbl(residual, "dispersion"),
              `Semantic coherence` = map_dbl(semantic_coherence, mean),
              `Held-out likelihood` = map_dbl(eval_heldout, "expected.heldout")) %>%
    gather(Metric, Value, -K) %>%
    ggplot(aes(K, Value, color = Metric)) +
    geom_line(size = 1.5, alpha = 0.7, show.legend = FALSE) +
    facet_wrap(~Metric, scales = "free_y") +
    labs(x = "K (number of topics)",
         y = NULL,
         title = "Model diagnostics by number of topics",
         subtitle = subtitle) +
    theme_bw() +
    ggsave(write_path, width = 15)
}

plot_semCoherence_v_exclusivitiy <- function(k_result, k_topics, title, write_path) {
  k_result %>%
    select(K, exclusivity, semantic_coherence) %>%
    filter(K %in% k_topics) %>%
    unnest(cols = c(exclusivity, semantic_coherence)) %>%
    mutate(K = as.factor(K)) %>%
    ggplot(aes(semantic_coherence, exclusivity, color = K)) +
    geom_point(size = 2, alpha = 0.7) +
    labs(x = "Semantic coherence",
         y = "Exclusivity",
         title = title) + 
    theme_bw() +
    ggsave(write_path, width = 15)
}
