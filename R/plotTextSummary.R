library(dplyr)
library(magrittr)
library(ggplot2)
library(tidytext)
library(tidystm)
library(ggthemes)
library(patchwork)
library(ggraph)
library(tidygraph)

#
# plot_prevalence_w_top_words
#
plot_prevalence_w_top_words <- function(tbl_gamma) {
  tbl_gamma %>%
    top_n(20, gamma) %>%
    ggplot(aes(topic, gamma, label = terms, fill = as.factor(topic))) +
    geom_col(alpha = .8, color = "black", fill = "midnightblue", show.legend = FALSE) +
    geom_text(hjust = 0, nudge_y = 0.0005, size = 3,
              family = "IBMPlexSans") +
    coord_flip() +
    scale_y_continuous(expand = c(0,0),
                       limits = c(0, 0.14),
                       labels = scales::percent_format()) +
    theme_tufte(base_family = "IBMPlexSans", ticks = FALSE) +
    theme(plot.title = element_text(size = 16,
                                    family="IBMPlexSans-Bold"),
          plot.subtitle = element_text(size = 13)) +
    scale_fill_grey() + 
    labs(x = NULL, y = expression(gamma),
         title = "Top 20 topics by prevalence",
         subtitle = "With top words that contribute to each topic")
}

#
# plot_top_words_by_mixtures
# input: tidy beta as output from tidytext, how the scales should behave for facet_wrap
#        and the list of topics of interests
#
plot_top_words_by_mixtures <- function(tbl_beta, scales, list_topics) {
  tbl_beta %>%
    filter(topic %in% list_topics) %>% 
    group_by(topic) %>%
    top_n(12) %>%
    ungroup %>%
    mutate(topic = as.factor(topic),
           term = reorder_within(term, beta, topic)) %>%
    ggplot(aes(term, beta, fill = topic)) +
    geom_col(show.legend = FALSE) +
    facet_wrap(~topic, scales = scales) +
    coord_flip() +
    scale_x_reordered() +
    scale_y_continuous(expand = c(0,0))
}

#
# plot_prevalence_tidy
#
plot_prevalence_tidy <- function(prep_stm, model_fit, scales, label_tbl = FALSE) {
  neworder = prep_stm$topics
  if (label_tbl == FALSE) {
    # tidy topic prevalence
    tmp <- extract.estimateEffect(prep_stm, "date", model = model_fit, method = "continuous") %>% 
      mutate(label = factor(topic, levels=neworder))
  } else {
    tmp <- extract.estimateEffect(prep_stm, "date", model = model_fit, method = "continuous") %>% 
      select(-label) %>% 
      left_join(label_tbl, by = "topic")
  }
  
  tmp %>%
    ggplot(aes(x = covariate.value, y = estimate,
               ymin = ci.lower, ymax = ci.upper,
               fill = "mignightblue")) +
    facet_wrap(~label, scales = scales) +
    geom_ribbon(alpha = .5, show.legend = FALSE) +
    geom_line(show.legend = FALSE) + 
    labs(x = NULL, y = "Expected Topic Proportion") +
    theme_bw() +
    scale_fill_grey()
}

# plot_graph
#
# input: corr_df, list_topics(how many topics), which_topics(output get_list_topics fun), threshold, title
# returns: plot where edges represents how likely two topics appear together in a doc
#          and only filtered nodes, given threshold, appear
#
plot_graph <- function(df_corr, list_topics, which_topic, threshold, title) {
  
  as_tbl_graph(df_corr$cor) %>% 
    mutate(topic = which_topic,
           is_relevant = ifelse(topic %in% list_topics$from, TRUE, FALSE)) %>% 
    activate(edges) %>% 
    filter(weight > threshold) %>% 
    ggraph("kk") +
    geom_edge_link(aes(edge_colour = weight)) +
    ggtitle(title) + 
    scale_edge_colour_viridis() +
    geom_node_point(aes(filter = is_relevant == TRUE)) +
    geom_node_label(aes(label = topic, filter = is_relevant == TRUE), 
                    repel = T) +
    theme_graph(title_size = 11)
}

#
# calc_p1: subsidiary fun for plot_summary
#
calc_p1 <- function(fit, covar) {
  td_beta <- tidy(fit, "beta")
  td_gamma <- tidy(fit, "gamma", document_names = covar$message_id)
  top_terms <- get_top_terms(td_beta)
  gamma_terms <- get_gamma_terms(td_gamma, top_terms)
  p1 <- plot_prevalence_w_top_words(gamma_terms)
  return(p1)
}

#
# plot_summary: quick fun to visualize an STM object
# input: an STM fit object and related metadata.
# return: plot with top 20 terms and related topic prevalence
#
plot_summary <- function(fit, covar) {
  
  p1 <- calc_p1(fit, covar)
  
  list_top20 <<- p1$data$topic %>% 
    str_remove("Topic ") %>% 
    as.numeric()
  
  prep <- estimateEffect(list_top20 ~ s(date), # this step can take a while
                         fit, 
                         covar, 
                         uncertainty = "Global")
  p2 <- plot_prevalence_tidy(prep, fit, "free_y")
  p1 + p2
}

#
# plot_wordCount_byDecade
#
plot_wordCount_byDecade <- function(df) {
  df %>% 
    group_by(decade) %>% 
    mutate(count_word = str_count(text, "\\b\\w+\\b")) %>% 
    ungroup() %>% 
    ggplot(aes(count_word, color = factor(decade), fill = factor(decade))) +
    geom_histogram(bins = 75, alpha = .7) + 
    scale_x_continuous(labels = scales::label_number()) +
    theme_bw()
}

#
# plot_log_odds_byMixture
# inputs: take a tidy dataframe and a type of mixture (decade or community)
# return: plot top 15 weighted log odds
#
plot_log_odds_byMixture <- function(tidy_df, mixture) {
  if (mixture == "community") {
    tidy_df %>%
      count(word, community, sort = T) %>% 
      tidylo::bind_log_odds(community, word, n) %>% 
      group_by(community) %>%
      top_n(15) %>%
      ungroup %>%
      mutate(community = as.factor(community),
             word = reorder_within(word, n, community)) %>%
      ggplot(aes(word, n, fill = community)) +
      geom_col(show.legend = FALSE) +s
    facet_wrap(~community, scales = "free_y") +
      coord_flip() +
      scale_x_reordered() +
      scale_y_continuous(expand = c(0,0))
  } else if (mixture == "month"){
    tidy_df %>%
      count(word, month, sort = T) %>% 
      tidylo::bind_log_odds(month, word, n) %>% 
      group_by(month) %>%
      top_n(15) %>%
      ungroup %>%
      mutate(month = as.factor(month),
             word = reorder_within(word, n, month)) %>%
      ggplot(aes(word, n, fill = month)) +
      geom_col(show.legend = FALSE) +
      facet_wrap(~month, scales = "free_y") +
      coord_flip() +
      scale_x_reordered() +
      scale_y_continuous(expand = c(0,0))
  }
  
}

