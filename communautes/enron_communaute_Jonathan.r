############################################
# Classement des courriels et analyse td_idf selon les communautés de Jonathan Saint-Onge. 
# Ses résultats se trouvent dans vertex_df_with_all_SBMs_community.csv
############################################
# 1 - Chaque courriel unique est catégorisé dans une communauté.
#    Trois catégorisations sont considérés:
#         (i) Si un courriel part de la communauté A et s'adresse exclusivement 
#         à des membres de la communauté A, le courriel est classé comme 
#         appartenant à la communauté A. Sinon, le courriel est inter-communauté (999)
#         (ii) L'appartenance du courriel est celle de l'envoyeur. 
#         (iii) courriels qui ne sont pas exclusivement à la communauté
# 2 - Sur ces nouveaux corpus, le code calcule la tf_idf, log_odds, bigram, trigram
#     Plusieurs fichiers sont exporter pour que Philippe Nazair en fasse une plateforme.
#         (i) méthode_noeuds.csv: commnunauté de chaque employé
#         (ii) méthode_message.csv: communauté de chaque courriel
#         (iii) méthode_calcul.jpg: image top 15 pour la tf_idf, log_odds,bigram,trigram
############################################
# Louis Renaud-Desjardins, Cirst, Bin, Janvier 2020
############################################
setwd('/home/louis/Desktop/TestGitLab/test_enron_louis')

library(tidyverse)
library(ggraph)
library(tidygraph)
library(tidytext)
library(igraph)
library(reticulate)
library(igraph)
library(magrittr)
library(visNetwork)
library(data.table)

# importation des données de fichier.pkl
# il est nécessaire d'utiliser python avec reticulate
use_python("~/anaconda3/bin/python3")
pd <- import("pandas")


############################################
# 1 - Communauté des courriels
############################################
# importation du réseaux de noeuds de Jonathan
noeuds <- read.csv('vertex_df_with_all_SBMs_community.csv')
noeuds <- select(noeuds,-X,-is.core) # on ne garde que les colonnes nécessaires
noeuds <- rename(noeuds,email=name)

# importation du corpus des courriels
corpus <- pd$read_pickle(paste0("/home/louis/Desktop/Enron/ssn_enron/enron_semantic_data.pkl"))
email_info <- pd$read_pickle(paste0("/home/louis/Desktop/Enron/ssn_enron/enron_social_data.pkl")) %>%
  # enlève les non-core, i.e. les employés récurrents seulement
  subset(core_receiver_name != 'NaN' & core_sender_name != 'NaN') %>%  
  select(message_id,sender_id,receiver_id) %>% 
  rename(from = sender_id, to=receiver_id) 
courriel <- merge(email_info,corpus,by='message_id')
rm(corpus,email_info)

methodes <- colnames(noeuds)[3:ncol(noeuds)]
n_courriel_unique <- length(unique(courriel$message_id))
email_community <- data.frame(tmp=integer(n_courriel_unique))

for (i in 1:length(methodes)){# 
  tmp_from <- select(noeuds,email,methodes[i])%>% rename(from=email) # communauté du receveur
  tmp_to <- select(noeuds,email,methodes[i])%>% rename(to=email) # communauté de l'envoyeur
  tmp_email <- courriel %>% merge(tmp_from,by='from') %>% rename(from_community = methodes[i]) %>%
    merge(tmp_to,by='to')%>% rename(to_community = methodes[i]) # ajout aux data
  tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des courriels strictement intra (i)
    group_by(message_id)%>% 
    summarise(wt = ifelse(mean(from_community)==mean(to_community),mean(from_community),999))%>%
    ungroup()
  if (i==1){email_community['message_id']<-tmp$message_id}
  email_community[paste(methodes[i],'_intra')] <- tmp$wt
  tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des communautés méthode (ii)
    group_by(message_id)%>%
    summarise(wt = mean(from_community))%>%
    ungroup()
  email_community[paste(methodes[i],'_sender')] <- tmp$wt
  tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des communautés méthode (ii)
    group_by(message_id)%>%
    summarise(wt = ifelse(mean(from_community)!=mean(to_community),mean(from_community),999))%>%
    ungroup()
  email_community[paste(methodes[i],'_extra')] <- tmp$wt 
}
rm(tmp_from,tmp_to,tmp_email,tmp)
email_community <- select(email_community,-tmp)

# fichier .csv
write.csv(noeuds,file="email_community_Jonathan.csv",row.names = FALSE)

############################################
# 2 - tf_idf, log_odds, bigram, trigram
############################################
tmp <- select(courriel,message_id,message) %>%
  merge(email_community,by='message_id')%>%
  unnest_tokens(word,message)
tmp_bi <- select(courriel,message_id,message) %>%
  merge(email_community,by='message_id')%>%
  unnest_tokens(word,message,token='ngrams',n=2)
tmp_tri <- select(courriel,message_id,message) %>%
  merge(email_community,by='message_id')%>%
  unnest_tokens(word,message,token='ngrams',n=3)

for (i in 2:ncol(email_community)){#
  tmp2 <- tmp 
  fn <- colnames(tmp2)[i]
  print(fn)
  colnames(tmp2)[i] <- 'algo'
  #tf-idf
  tmp2%>%count(algo,word)%>%
    bind_tf_idf(word,algo,n) %>%
    arrange(desc(tf_idf))%>% mutate(word = factor(word, levels = rev(unique(word)))) %>% 
    group_by(algo) %>% 
    top_n(15) %>% 
    ungroup() %>%
    ggplot(aes(word, tf_idf, fill = algo)) +
    geom_col(show.legend = FALSE) +
    labs(x = NULL, y = "tf-idf") +
    facet_wrap(~algo, ncol = 2, scales = "free") +
    coord_flip()
  ggsave(filename = paste('Graphiques/',fn,'_tfidf.jpg'),width = 6 ,height = 15,dpi=300)
  #bigram
  tmp2 <- tmp_bi 
  colnames(tmp2)[i] <- 'algo'
  tmp2 %>%count(algo,word)%>%
    bind_tf_idf(word,algo,n) %>%
    arrange(desc(tf_idf))%>% mutate(word = factor(word, levels = rev(unique(word)))) %>% 
    group_by(algo) %>% 
    top_n(15) %>% 
    ungroup() %>%
    ggplot(aes(word, tf_idf, fill = algo)) +
    geom_col(show.legend = FALSE) +
    labs(x = NULL, y = "tf-idf") +
    facet_wrap(~algo, ncol = 2, scales = "free") +
    coord_flip()
  ggsave(filename = paste('Graphiques/',fn,'_bigram.jpg'),width = 8 ,height = 15,dpi=300)
  #trigram
  tmp2 <- tmp_tri 
  colnames(tmp2)[i] <- 'algo'
  tmp2 %>%count(algo,word)%>%
    bind_tf_idf(word,algo,n) %>%
    arrange(desc(tf_idf))%>% mutate(word = factor(word, levels = rev(unique(word)))) %>% 
    group_by(algo) %>% 
    top_n(15) %>% 
    ungroup() %>%
    ggplot(aes(word, tf_idf, fill = algo)) +
    geom_col(show.legend = FALSE) +
    labs(x = NULL, y = "tf-idf") +
    facet_wrap(~algo, ncol = 2, scales = "free") +
    coord_flip()
  ggsave(filename = paste('Graphiques/',fn,'_trigram.jpg'),width = 10 ,height = 15,dpi=300)
}




