############################################
# Detection de communauté pour Enron. 
############################################
# 1 - Divise les auteurs des courriels en communautés basées sur leurs envois. 
#     Deux algorithmes sont utilisés: fastgreedy, walktrap
# 2 - Chaque courriel unique est catégorisé dans une communauté.
#     Trois catégorisations sont considérés:
#         (i) Si un courriel part de la communauté A et s'adresse exclusivement 
#         à des membres de la communauté A, le courriel est classé comme 
#         appartenant à la communauté A. Sinon, le courriel est inter-communauté (999)
#         (ii) L'appartenance du courriel est celle de l'envoyeur. 
#         (iii) courriels qui ne sont pas exclusivement à la communauté
# 3 - Sur ces nouveaux corpus, le code calcule la tf_idf, log_odds, bigram, trigram
#     Plusieurs fichiers sont exporter pour que Philippe Nazair en fasse une plateforme.
#         (i) méthode_noeuds.csv: commnunauté de chaque employé
#         (ii) méthode_message.csv: communauté de chaque courriel
#         (iii) méthode_calcul.jpg: image top 15 pour la tf_idf, log_odds,bigram,trigram
############################################
# Louis Renaud-Desjardins, Cirst, Bin, Decembre 2019
############################################

library(tidyverse)
library(ggraph)
library(tidygraph)
library(tidytext)
library(igraph)
library(reticulate)
library(igraph)
library(magrittr)
library(visNetwork)
library(data.table)

############################################
# 1 - Détection de Communautés
############################################

# importation des données de fichier.pkl
# il est nécessaire d'utiliser python avec reticulate
use_python("~/anaconda3/bin/python3")
pd <- import("pandas")
data <- pd$read_pickle(paste0("/home/louis/Desktop/Enron/ssn_enron/enron_social_data.pkl")) %>%
  # enlève les non-core, i.e. les employés récurrents seulement
  subset(core_receiver_name != 'NaN' & core_sender_name != 'NaN') %>%  
  # seulement 4 variables sont utiles
  select(sender_id,receiver_id,core_sender_status,core_receiver_status) %>% 
  rename(from = sender_id, to=receiver_id,from_status=core_sender_status,to_status=core_receiver_status) 

# définition de nos noeuds: employé unique
# email de l'employé et status
n1 <- select(data,from,from_status) %>%
  rename(email=from,status=from_status)
n2 <- select(data,to,to_status) %>%
  rename(email=to,status=to_status)
noeuds <- unique(rbind(n1,n2))
noeuds <- noeuds[order(noeuds$email),] # ordre croissant des courriels
rm(n1,n2)

# définition des liens: chaque courriel d'un email à un autre est un lien
liens <- count(select(data,from,to),from,to) %>% rename(weight=n)

# détection des communautés
g <- graph.data.frame(liens, directed=F)
g <- simplify(g)
# algorithme fastgreedy: non-dirigé, mesure de poids
fc <- fastgreedy.community(g)
noeuds$fastgreedy <- fc$membership
# algorithme walktrap: non-dirigé, mesure de poids
wt <- walktrap.community(g)
noeuds$walktrap <- wt$membership
noeuds$walktrap5 <- cutat(wt, 5)
rm(g,fc,wt)

# fichier .csv
write.csv(noeuds,file="noeuds_Louis.csv",row.names = FALSE)

############################################
# 2 - Communauté des courriels
############################################
# importation du corpus des courriels
corpus <- pd$read_pickle(paste0("/home/louis/Desktop/Enron/ssn_enron/enron_semantic_data.pkl"))
email_info <- pd$read_pickle(paste0("/home/louis/Desktop/Enron/ssn_enron/enron_social_data.pkl")) %>%
  # enlève les non-core, i.e. les employés récurrents seulement
  subset(core_receiver_name != 'NaN' & core_sender_name != 'NaN') %>%  
  select(message_id,sender_id,receiver_id) %>% 
  rename(from = sender_id, to=receiver_id) 
courriel <- merge(email_info,corpus,by='message_id')
rm(corpus,email_info)

# assignation des communautés pour fastgreedy
tmp_from <- select(noeuds,email,fastgreedy)%>% rename(from=email) # communauté du receveur
tmp_to <- select(noeuds,email,fastgreedy)%>% rename(to=email) # communauté de l'envoyeur
tmp_email <- courriel %>% merge(tmp_from,by='from') %>% rename(from_community = fastgreedy) %>%
  merge(tmp_to,by='to')%>% rename(to_community = fastgreedy) # ajout aux data
email_community <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des courriels strictement intra (i)
  group_by(message_id)%>% 
  summarise(fastgreedy_intra = ifelse(mean(from_community)==mean(to_community),mean(from_community),999))%>%
  ungroup()
tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des communautés méthode (ii)
  group_by(message_id)%>%
  summarise(fg = mean(from_community))%>%
  ungroup()
email_community$fastgreedy_sender <- tmp$fg
tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des communautés méthode (ii)
  group_by(message_id)%>%
  summarise(fg = ifelse(mean(from_community)!=mean(to_community),mean(from_community),999))%>%
  ungroup()
email_community$fastgreedy_extra <- tmp$fg
rm(tmp_from,tmp_to,tmp_email,tmp)

# compte pour vérification
#a <- count(select(email_community,fastgreedy_intra,message_id),fastgreedy_intra)
#b <- count(select(email_community,fastgreedy_sender,message_id),fastgreedy_sender)
#c <- count(select(email_community,fastgreedy_extra,message_id),fastgreedy_extra)

# assignation des communautés pour walktrap
tmp_from <- select(noeuds,email,walktrap)%>% rename(from=email) # communauté du receveur
tmp_to <- select(noeuds,email,walktrap)%>% rename(to=email) # communauté de l'envoyeur
tmp_email <- courriel %>% merge(tmp_from,by='from') %>% rename(from_community = walktrap) %>%
  merge(tmp_to,by='to')%>% rename(to_community = walktrap) # ajout aux data
tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des courriels strictement intra (i)
  group_by(message_id)%>% 
  summarise(wt = ifelse(mean(from_community)==mean(to_community),mean(from_community),999))%>%
  ungroup()
email_community$walktrap_intra <- tmp$wt
tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des communautés méthode (ii)
  group_by(message_id)%>%
  summarise(wt = mean(from_community))%>%
  ungroup()
email_community$walktrap_sender <- tmp$wt
tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des communautés méthode (ii)
  group_by(message_id)%>%
  summarise(wt = ifelse(mean(from_community)!=mean(to_community),mean(from_community),999))%>%
  ungroup()
email_community$walktrap_extra <- tmp$wt
rm(tmp_from,tmp_to,tmp_email,tmp)

# assignation des communautés pour walktrap5
tmp_from <- select(noeuds,email,walktrap5)%>% rename(from=email) # communauté du receveur
tmp_to <- select(noeuds,email,walktrap5)%>% rename(to=email) # communauté de l'envoyeur
tmp_email <- courriel %>% merge(tmp_from,by='from') %>% rename(from_community = walktrap5) %>%
  merge(tmp_to,by='to')%>% rename(to_community = walktrap5) # ajout aux data
tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des courriels strictement intra (i)
  group_by(message_id)%>% 
  summarise(wt = ifelse(mean(from_community)==mean(to_community),mean(from_community),999))%>%
  ungroup()
email_community$walktrap5_intra <- tmp$wt
tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des communautés méthode (ii)
  group_by(message_id)%>%
  summarise(wt = mean(from_community))%>%
  ungroup()
email_community$walktrap5_sender <- tmp$wt
tmp <- select(tmp_email,message_id,from_community,to_community) %>% # assignation des communautés méthode (ii)
  group_by(message_id)%>%
  summarise(wt = ifelse(mean(from_community)!=mean(to_community),mean(from_community),999))%>%
  ungroup()
email_community$walktrap5_extra <- tmp$wt
rm(tmp_from,tmp_to,tmp_email,tmp)

# fichier .csv
write.csv(noeuds,file="email_community_Louis.csv",row.names = FALSE)


############################################
# 3 - tf_idf, log_odds, bigram, trigram
############################################
tmp <- select(courriel,message_id,message) %>%
  merge(email_community,by='message_id')%>%
  unnest_tokens(word,message)
tmp_bi <- select(courriel,message_id,message) %>%
  merge(email_community,by='message_id')%>%
  unnest_tokens(word,message,token='ngrams',n=2)
tmp_tri <- select(courriel,message_id,message) %>%
  merge(email_community,by='message_id')%>%
  unnest_tokens(word,message,token='ngrams',n=3)

for (i in 2:ncol(email_community)){
  tmp2 <- tmp 
  fn <- colnames(tmp2)[i]
  print(fn)
  colnames(tmp2)[i] <- 'algo'
  #tf-idf
  tmp2%>%count(algo,word)%>%
    bind_tf_idf(word,algo,n) %>%
    arrange(desc(tf_idf))%>% mutate(word = factor(word, levels = rev(unique(word)))) %>% 
    group_by(algo) %>% 
    top_n(15) %>% 
    ungroup() %>%
    ggplot(aes(word, tf_idf, fill = algo)) +
    geom_col(show.legend = FALSE) +
    labs(x = NULL, y = "tf-idf") +
    facet_wrap(~algo, ncol = 2, scales = "free") +
    coord_flip()
  ggsave(filename = paste(fn,'_tfidf.jpg'),width = 6 ,height = 15,dpi=300)
  #bigram
  tmp2 <- tmp_bi 
  colnames(tmp2)[i] <- 'algo'
  tmp2 %>%count(algo,word)%>%
    bind_tf_idf(word,algo,n) %>%
    arrange(desc(tf_idf))%>% mutate(word = factor(word, levels = rev(unique(word)))) %>% 
    group_by(algo) %>% 
    top_n(15) %>% 
    ungroup() %>%
    ggplot(aes(word, tf_idf, fill = algo)) +
    geom_col(show.legend = FALSE) +
    labs(x = NULL, y = "tf-idf") +
    facet_wrap(~algo, ncol = 2, scales = "free") +
    coord_flip()
  ggsave(filename = paste(fn,'_bigram.jpg'),width = 8 ,height = 15,dpi=300)
  #trigram
  tmp2 <- tmp_tri 
  colnames(tmp2)[i] <- 'algo'
  tmp2 %>%count(algo,word)%>%
    bind_tf_idf(word,algo,n) %>%
    arrange(desc(tf_idf))%>% mutate(word = factor(word, levels = rev(unique(word)))) %>% 
    group_by(algo) %>% 
    top_n(15) %>% 
    ungroup() %>%
    ggplot(aes(word, tf_idf, fill = algo)) +
    geom_col(show.legend = FALSE) +
    labs(x = NULL, y = "tf-idf") +
    facet_wrap(~algo, ncol = 2, scales = "free") +
    coord_flip()
  ggsave(filename = paste(fn,'_trigram.jpg'),width = 10 ,height = 15,dpi=300)
}


# EXTRA: clean the text/messages/emails
x %>%
  str_remove(regex("forwarded.+?subject", multiline = TRUE)) %>% 
  str_remove(regex("\\<.+?@.+?\\>.+?to:.+?subject:", multiline = TRUE)) %>%
  str_replace_all("[^[:alnum:]]", " ") %>% 
  str_replace_all("[[:digit:]]+", " ") %>% 
  str_replace_all("\\s+", " ")

# custom stop_words by JoSO
s_w <- c("fw", "messagefrom", "amto", "mmbtud", "pge", "don", "iso", "tw",
          "dwr", "srp", "ng", "scs", "tep", "epe", "gfex", "pl", "mw", "-PRON-", ".",
          "pm", "etran", "forward", "nicolayect", "pmto", "clyneshouectect", "sewellhouectect",
          "xla", "okif", "fyi", "idt", "af", "al", "cc", "da", "apx", "px", "trv", "internetusato",
          "associateanalyst", "dt", "agg", "ena", "nx", "cng", "sp", "ve", "alot", "boyt",
          "infobelow", "ll", "nimo", "thisrebecca", "edt", "aug", "ecom", "cthis",
          "thesender", "eknock", "ofo", "33am", "hou", "jeff", "p.m", "week", "s", "by",
          "for", "08", "17", "hou", "02", "15", "ectcc", "lon", "ect", "original message",
          "ees", "713", "646", "713", "853", "enroncc", "ca", "iso cal", "px" , "re", "kh")

tmp <- soc_sem %>%
  unnest_tokens(word, text) %>% # remove punctuation, retains linenumbers, and lower case
  anti_join(stop_words)
