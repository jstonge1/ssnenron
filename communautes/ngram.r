############################################
# Detection des n-gram qui reviennent le plus dans le but de nettoyer les courriels. 
############################################
# je considère ici les n-gram de 5 à 15
############################################
# Louis Renaud-Desjardins, Cirst, Bin, Janvier 2020
############################################

library(tidytext)
library(reticulate)

# importation des données de fichier.pkl
# il est nécessaire d'utiliser python avec reticulate
use_python("~/anaconda3/bin/python3")
pd <- import("pandas")
corpus <- pd$read_pickle(paste0("/home/louis/Desktop/Enron/ssn_enron/enron_semantic_data.pkl"))
courriels <- unique(corpus$message)

ngrams <- data.frame(tmp=integer(20))

for (i in 5:15){
  tmp <- courriels %>%
    unnest_tokens(word,message,token='ngrams',n=i)%>%
    count()%>%
    arrange(desc(n))
  ngrams[toString(i)]<-tmp[1:20,1]
  ngrams[paste(n,i)]<-tmp[1:20,2]
}

write.csv(noeuds,file="ngrams.csv",row.names = FALSE)
