# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 19:30:32 2019

@author: Max
"""

import spacy
import gensim
import pyLDAvis.gensim
from gensim.models import CoherenceModel
from gensim.utils import lemmatize, simple_preprocess
from gensim.corpora.dictionary import Dictionary
import gensim.corpora as corpora
from pprint import pprint
import re
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation, NMF
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import sklearn.metrics.pairwise as pw
import numpy as np
import scipy as sp
import treetaggerwrapper
import platform

pd.set_option('display.max_rows', 50)
pd.set_option('display.max_columns', 10)
pd.set_option('display.width', 150)

#tree_tagger_dir = '/Users/Maxime/Dropbox/programmes/TreeTaggerOS'

# tagger = treetaggerwrapper.TreeTagger(
#    TAGLANG='en', TAGDIR=tree_tagger_dir, TAGINENC='utf-8', TAGOUTENC='utf-8')


########################################
######### LOADING PROCEDURES ###########
########################################

def load_enron_social_data():
    return pd.read_pickle('ssn_data/enron_social_data.p')


def load_enron_semantic_data():
    return pd.read_pickle('ssn_data/enron_semantic_data.p')


def load_enron_data():
    social_data = load_enron_social_data()
    semantic_data = load_enron_semantic_data()
    enron_data = pd.merge(social_data,
                          semantic_data,
                          on='message_id',
                          how='left')
    return enron_data


def load_3gram_sender_matrix():
    data = pd.read_pickle('sem_data/ngram_sender_matrix.p')
    # convert matrix back to wide form
    data = data.pivot(index='sender_id',
                      columns='ngram',
                      values='tfidf_score')
    data = data.fillna(0)
    return data


##############################################
########### GENERATION PROCEDURES ############
##############################################

def create_3gram_sender_matrix():
    # matrix = email.id x ngrams of all messages
    corpus = load_enron_data()
    # create 3-gram tfidf vectorizer
    vector = TfidfVectorizer(analyzer='char_wb',
                             ngram_range=(3, 3))
    # merge messages by sender
    corpus = corpus.groupby(
        ['sender_id'])['message'].apply(lambda x: ' '.join(x))
    corpus = pd.DataFrame(corpus)
    print('Converting corpus to matrix')
    # create matrix
    matrix = vector.fit_transform(corpus['message'])
    # use ngrams as matrix columns
    lexicon = vector.get_feature_names()
    # use sender emails as document ids
    documents = corpus.index
    # convert matrix to dataframe with row and column labels
    matrix = pd.DataFrame(matrix.toarray(),
                          columns=lexicon,
                          index=documents)
    matrix = matrix.stack().reset_index()
    matrix.columns = ['sender_id', 'ngram', 'tfidf_score']
    # remove empty ngram values for efficient storage
    matrix = matrix[matrix['tfidf_score'] > 0]
    matrix.to_pickle('sem_data/ngram_sender_matrix.p')
    return matrix


def create_word_sender_matrix():
    # this function create a "sender" x 3grams of all sent emails"
    corpus = load_enron_data()
    # create 3-gram tfidf vectorizer
    vector = TfidfVectorizer(analyzer='word')
    # merge messages by sender
    corpus = corpus.groupby(
        ['sender_id'])['message'].apply(lambda x: ' '.join(x))
    corpus = pd.DataFrame(corpus)
    print('Converting corpus to matrix')
    # create matrix
    matrix = vector.fit_transform(corpus['message'])
    # use ngrams as matrix columns
    lexicon = vector.get_feature_names()
    # use sender emails as document ids
    documents = corpus.index
    # convert matrix to dataframe with row and column labels
    matrix = pd.DataFrame(matrix.toarray(),
                          columns=lexicon,
                          index=documents)
    matrix = matrix.stack().reset_index()
    matrix.columns = ['sender_id', 'word', 'tfidf_score']
    # remove empty ngram values for efficient storage
    matrix = matrix[matrix['tfidf_score'] > 0]
    matrix.to_pickle('sem_data/word_sender_matrix.p')
    return matrix


def create_dist_matrix(this_matrix):
    # this function takes create_3gram_sender_matrix() as this_matrix param
    # matrix  = email.id x email.id
    # TODO: Run this matrix w/ jonathan's social matrix
    dist_matrix = pd.DataFrame(pw.pairwise_distances(this_matrix,
                                                     metric='cosine'))
    dist_matrix.columns = this_matrix.index
    dist_matrix.index = this_matrix.index
    # convert matrix to long form
    dist_matrix = dist_matrix.stack().reset_index(1)
    dist_matrix.columns = ['sender_id2',
                           'cosine_dist']
    dist_matrix = dist_matrix.reset_index()
    # remove distances from self
    dist_matrix = dist_matrix[dist_matrix['sender_id']
                              != dist_matrix['sender_id2']]
    dist_matrix.to_pickle('sem_dist_data/ngram_cosine.p')
    return dist_matrix

######################################
########## My SCRIPT #################
######################################


soc_df = pd.read_feather("ssn_data/social_matrix.feather")
sem_data = pd.read_pickle("ssn_data/enron_semantic_data.p")

soc_df["message_id"] = soc_df["message_id"].astype("int")

soc_sem_data = pd.merge(soc_df, sem_data,
                        on='message_id', how='left')

soc_sem_data_subset = soc_sem_data.loc[:, ["from", "to", "deg.x", "deg.y",
                                           "community_sender", "community_receiver", "same_community",
                                           "message_id", "message"]]

soc_sem_data_subset["same_community"] = soc_sem_data_subset["same_community"].astype(
    "int").astype("category")

#####################################
######## LDA, NMF, SVD ##############
#####################################


documents = soc_sem_data_subset["message"]
no_features = 1000

# NMF needs tf-idf
tfidf_vectorizer = TfidfVectorizer(
    max_df=0.95, min_df=2, max_features=no_features, stop_words='english')
tfidf = tfidf_vectorizer.fit_transform(documents)
tfidf_feature_names = tfidf_vectorizer.get_feature_names()

tf_vectorizer = CountVectorizer(
    max_df=0.95, min_df=2, max_features=no_features, stop_words='english')
tf = tf_vectorizer.fit_transform(documents)
tf_feature_names = tf_vectorizer.get_feature_names()

# Coming from Balasubramanyan et al.
no_topics = 15

# run NMF
nmf = NMF(n_components=no_topics, random_state=1, alpha=.1,
          l1_ratio=.5, init='nndsvd').fit(tfidf)

# run LDA
lda = LatentDirichletAllocation(n_components=no_topics, max_iter=5,
                                learning_method='online', learning_offset=50., random_state=0).fit(tf)


def display_topics(model, feature_names, no_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic %d:" % (topic_idx))
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-no_top_words - 1:-1]]))


no_top_words = 10
display_topics(nmf, tfidf_feature_names, no_top_words)  # meaningless
display_topics(lda, tf_feature_names, no_top_words)

###########################
## Gensim implementation ##
###########################
# see https://www.machinelearningplus.com/nlp/topic-modeling-visualization-how-to-present-results-lda-models/


spacy_nlp = spacy.load('en_core_web_sm')
stop_words = spacy.lang.en.stop_words.STOP_WORDS

# 1)tokenize sentences and clean


def sent_to_words(sentences):
    for sent in sentences:
        sent = re.sub('\S*@\S*\s?', '', sent)  # remove emails
        sent = re.sub('\s+', ' ', sent)  # remove newline chars
        sent = re.sub("\'", "", sent)  # remove single quotes
        sent = gensim.utils.simple_preprocess(str(sent), deacc=True)
        yield(sent)

# Convert to list


data_words = list(sent_to_words(documents))
print(data_words[:1])

# 2) Build bigram, trigram models and lemmatize
# Build the bigram and trigram models
# higher threshold fewer phrases.
bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)
trigram = gensim.models.Phrases(bigram[data_words], threshold=100)
bigram_mod = gensim.models.phrases.Phraser(bigram)
trigram_mod = gensim.models.phrases.Phraser(trigram)

# !python3 -m spacy download en  # run in terminal once


def process_words(texts, stop_words=stop_words, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    """Remove Stopwords, Form Bigrams, Trigrams and Lemmatization"""
    texts = [[word for word in simple_preprocess(
        str(doc)) if word not in stop_words] for doc in texts]
    texts = [bigram_mod[doc] for doc in texts]
    texts = [trigram_mod[bigram_mod[doc]] for doc in texts]
    texts_out = []
    nlp = spacy.load('en', disable=['parser', 'ner'])
    for sent in texts:
        doc = nlp(" ".join(sent))
        texts_out.append(
            [token.lemma_ for token in doc if token.pos_ in allowed_postags])
    # remove stopwords once more after lemmatization
    texts_out = [[word for word in simple_preprocess(
        str(doc)) if word not in stop_words] for doc in texts_out]
    return texts_out


data_ready = process_words(data_words)  # processed Text Data!

# 3) Build the topic model

# Create Dictionary
id2word = corpora.Dictionary(data_ready)

# Create Corpus: Term Document Frequency
corpus = [id2word.doc2bow(text) for text in data_ready]

# build Gensim LDa model
lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                            id2word=id2word,
                                            num_topics=no_topics,
                                            random_state=100,
                                            update_every=1,
                                            chunksize=10,
                                            passes=10,
                                            alpha='symmetric',
                                            iterations=100,
                                            per_word_topics=True)

pprint(lda_model.print_topics())

# trying multicore gensim with 9 topics
# values coming from Corneli et al. this time
lda_model_9_topic = gensim.models.ldamolde.LdaMulticore(corpus,
                                                        id2word=id2word,
                                                        num_topics=9,
                                                        random_state=100,
                                                        update_every=1,
                                                        chunksize=10,
                                                        passes=10,
                                                        alpha='symmetric',
                                                        iterations=100,
                                                        per_word_topics=True)

# What is dominant topic ?


def format_topics_sentences(ldamodel=None, corpus=corpus, texts=documents):
    # Init output
    sent_topics_df = pd.DataFrame()

    # Get main topic in each document
    for i, row_list in enumerate(ldamodel[corpus]):
        row = row_list[0] if ldamodel.per_word_topics else row_list
        # print(row)
        row = sorted(row, key=lambda x: (x[1]), reverse=True)
        # Get the Dominant topic, Perc Contribution and Keywords for each document
        for j, (topic_num, prop_topic) in enumerate(row):
            if j == 0:  # => dominant topic
                wp = ldamodel.show_topic(topic_num)
                topic_keywords = ", ".join([word for word, prop in wp])
                sent_topics_df = sent_topics_df.append(pd.Series(
                    [int(topic_num), round(prop_topic, 4), topic_keywords]), ignore_index=True)
            else:
                break
    sent_topics_df.columns = ['Dominant_Topic',
                              'Perc_Contribution', 'Topic_Keywords']

    # Add original text to the end of the output
    contents = pd.Series(texts)
    sent_topics_df = pd.concat([sent_topics_df, contents], axis=1)
    return(sent_topics_df)


df_topic_sents_keywords = format_topics_sentences(
    ldamodel=lda_model, corpus=corpus, texts=data_ready)

# Format
df_dominant_topic = df_topic_sents_keywords.reset_index()
df_dominant_topic.columns = [
    'Document_No', 'Dominant_Topic', 'Topic_Perc_Contrib', 'Keywords', 'Text']
df_dominant_topic.head(10)

# Most representative sentence for each topic
# Display setting to show more characters in column
pd.options.display.max_colwidth = 100

sent_topics_sorteddf_mallet = pd.DataFrame()
sent_topics_outdf_grpd = df_topic_sents_keywords.groupby('Dominant_Topic')

for i, grp in sent_topics_outdf_grpd:
    sent_topics_sorteddf_mallet = pd.concat([sent_topics_sorteddf_mallet,
                                             grp.sort_values(['Perc_Contribution'], ascending=False).head(1)],
                                            axis=0)

# Reset Index
sent_topics_sorteddf_mallet.reset_index(drop=True, inplace=True)

# Format
sent_topics_sorteddf_mallet.columns = [
    'Topic_Num', "Topic_Perc_Contrib", "Keywords", "Representative Text"]

# Show
sent_topics_sorteddf_mallet.head(10)

# pyLDAvis.enable_notebook()
vis = pyLDAvis.gensim.prepare(lda_model, corpus, dictionary=lda_model.id2word)
vis

# Save model
lda_model.save("./sem_data/lda_model_topic_15.model")
