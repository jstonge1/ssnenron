## Measures

   - degree and edge betweenness
   - centralities
   - communities: overlapping or not
   - connected components
   - modularity

## Mathematical Models

   - Random graph (Erdos-Renyi)
   - Small-world (Watts-Strogatz)
   - Growing networks (Barabasi-Albert)
     + preferential attachment
   - Block models (max-entropy ensembles)
     + stochastic
     + hierarchical
     + degree-corrected

## Monolayer/single Network

   - Static
   - Dynamic

## Multilayer Network

   - Static/multiplex (same set of nodes):
     + Transportation networks
       * train
       * bus
       * subway

   - Temporal  networks: each layer is a time slice.
      + Epidemic diffusion:
       * household
       * work
       * school

   - Networks of networks: each layer by a different set of nodes.

## Dynamical processes on multilayer networks

--------------------------------------

   - Case1 : two-layer multiplex network:
     + e.g. power grid and the internet (Buldyrev et al. 2010)

   - Case2: 
     + e.g. Plant-pollinator Network


--------------------------------------

## Socsemics???

   - Single network:
     + latent community OR latent topics
       * overlapping community or not.

   - Bipartite Graph[?]
     + type node 1: agents
     + type node 2: topics
     => Link between those if agent x talks about topic y. The network ceased to be about the individuals directly exchanging emails.

   - Temporal networks:
     + social community evolve over time.
     + topics of interest evolve over time.

   - Example of multilayer networks:
     + communication via email or cellphone.

----------------------------------

   - Bouveyron et al.
     + Co-clustering between individuals and objects while accounting for the textual content of their interactions.
       - Think plant-pollinator, but interactions have content!



-------------------------------------

Hypothesis:

 - About overlapping/non-overlapping communities:
   + We could test if the proportions whereby an agent belong to different communities change as they "visit" some topics, and vice versa. That is, as they engage with a specific topic they become more and more ingrained in one community [?] And as they belong to a particular community, they start talking more and more about a particular topic. So we should see some kind of dynamics toward an equilibrium (stable point) where topics and communities become well correlated. 
