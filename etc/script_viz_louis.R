#
# Script dataviz pour la rencontre du 6
# idéalement à rouler sur serveur starship pour que les paths fonctionnent
# les paths pourront être réglés une fois le nettoyage faites après le 6
#

list.of.packages <- c("tidygraph", "ggraph", "tidytext", "stm")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)

library(tidygraph)
library(ggraph)
library(tidyverse)
library(tidytext)
library(ggthemes)
library(stm)

setwd("~/ssnenron/")

# load data
enron_covar <- readRDS("/data/Jonathan/dat_enron/prep_models/enron_metadata4.rds")
enron_sparse <- readRDS("/data/Jonathan/dat_enron/prep_models/enron_sparse4.rds")
topic_model_70 <- readRDS("/data/Jonathan/dat_enron/fit/enron70_byDate_1_31_20.rds")
communities <- read_csv("/data/Jonathan/dat_enron/wdcNSBMexp.csv")  

# tidy matrix from tidytext
td_gamma70 <- tidy(topic_model_70, "gamma", document_names = rownames(enron_sparse))
td_beta70 <- tidy(topic_model_70, "beta")

###############
# HELPER FUNS #
###############
get_top_terms <- function(tbl_beta) {
  top_terms <- tbl_beta %>%
    arrange(beta) %>%
    group_by(topic) %>%
    top_n(20, beta) %>%
    arrange(-beta) %>%
    select(topic, term) %>%
    summarise(terms = list(term)) %>%
    mutate(terms = map(terms, paste, collapse = ", ")) %>% 
    unnest(cols = c(terms))
  
  return(top_terms)
}

get_gamma_terms <- function(tbl_gamma, top_terms) {
  prep_gamma <- tbl_gamma %>%
    group_by(topic) %>%
    summarise(gamma = mean(gamma)) %>%
    arrange(desc(gamma)) %>%
    left_join(top_terms, by = "topic") %>%
    mutate(topic = paste0("Topic ", topic),
           topic = reorder(topic, gamma))
  
  return(prep_gamma)
}

##############
# GRAPH FUNS #
##############
plot_prevalence_w_top_words <- function(tbl_gamma) {
  tbl_gamma %>%
    top_n(20, gamma) %>%
    ggplot(aes(topic, gamma, label = terms, fill = as.factor(topic))) +
    geom_col(alpha = .8, color = "black", fill = "midnightblue", show.legend = FALSE) +
    geom_text(hjust = 0, nudge_y = 0.0005, size = 3,
              family = "IBMPlexSans") +
    coord_flip() +
    scale_y_continuous(expand = c(0,0),
                       limits = c(0, 0.14),
                       labels = scales::percent_format()) +
    theme_tufte(base_family = "IBMPlexSans", ticks = FALSE) +
    theme(plot.title = element_text(size = 16,
                                    family="IBMPlexSans-Bold"),
          plot.subtitle = element_text(size = 13)) +
    scale_fill_grey() + 
    labs(x = NULL, y = expression(gamma),
         title = "Top 20 topics by prevalence",
         subtitle = "With top words that contribute to each topic")
}

plot_top_words_by_mixtures <- function(tbl_beta, scales, list_topics) {
  tbl_beta %>%
    filter(topic %in% list_topics) %>% 
    group_by(topic) %>%
    top_n(12) %>%
    ungroup %>%
    mutate(topic = as.factor(topic),
           term = reorder_within(term, beta, topic)) %>%
    ggplot(aes(term, beta, fill = topic)) +
    geom_col(show.legend = FALSE) +
    facet_wrap(~topic, scales = scales) +
    coord_flip() +
    scale_x_reordered() +
    scale_y_continuous(expand = c(0,0))
}

#####################
# TOP 20 PREVALENCE #
#####################

top_terms <- get_top_terms(td_beta70)
gamma_terms <- get_gamma_terms(td_gamma70, top_terms)
gamma_terms <- gamma_terms %>% 
  # Liste des topics à enlever provient d'une examination manuelle
  # ce sont des topics qui semblent trop générales
  filter(!topic %in% paste0("Topic ", c(54, 69, 55, 36, 32, 21, 41, 37, 39, 42,
                                        1, 29, 34, 59, 63, 70, 2, 40)))

plot_prevalence_w_top_words(gamma_terms) 

# ggsave("enron70_top20_01_31_20.png", width = 14)

################
# TOP MIXTURES #
################

# if exploring enron 70 topic, we need to subet
enron70_topterms <- get_top_terms(td_beta70)
enron70_gammaterms <- get_gamma_terms(td_gamma70, enron70_topterms)

list70_topGamma_topics <- enron70_gammaterms %>% 
  mutate(topic = str_remove_all(topic, "Topic "),
         topic = as.numeric(topic)) %>%
  filter(!topic %in% c(54, 69, 55, 36, 32, 21, 41, 37, 39, 42,
                       1, 29, 34, 59, 63, 70, 2, 40)) %>%
  slice(1:20) %>%
  pull(topic)

plot_top_words_by_mixtures(td_beta70, "free_y", list70_topGamma_topics)
# ggsave("figs/enron70_topMixture_01_31_20.png", width = 9)
###########
# HEATMAP #
###########

# voir script build_plant_pol_matrix.R

##############
# TOPIC CORR #
##############

enron70_topicCorr <- stm::topicCorr(topic_model_70)

relevant_edges <- as_tbl_graph(enron70_topicCorr$cor) %>% 
  activate(edges) %>% 
  filter(weight > .1) %>% 
  as_tibble() %>% 
  filter(from != to)

relevant_from <- relevant_edges %>% 
  distinct(from)

relevant_to <- relevant_edges %>% 
  distinct(to) %>% rename(from=to)

list_topics <- relevant_from %>% 
  bind_rows(relevant_to) %>% 
  distinct() %>% 
  filter(!from %in% c(54, 69, 55, 36, 32, 21, 41, 37, 39, 42, 
                      1, 29, 34, 59, 63, 70, 2, 40))

as_tbl_graph(enron70_topicCorr$cor) %>%
  activate(nodes) %>% 
  mutate(
    topic = 1:70,
    is_relevant = ifelse(topic %in% list_topics$from, TRUE, FALSE)
  ) %>%
  activate(edges) %>%
  mutate(
    is_relevant_from = ifelse(from %in% list_topics$from, TRUE, FALSE),
    is_relevant_to = ifelse(to %in% list_topics$from, TRUE, FALSE),
    is_relevant = ifelse(is_relevant_from & is_relevant_to == TRUE, TRUE, FALSE)
  ) %>% 
  filter(weight > .1) %>%
  ggraph() +
  geom_edge_link(aes(edge_colour = weight,
                     filter = is_relevant == TRUE)) +
  labs(title = "Enron (70) Topic Correlation (threshold > 0.1)", 
       subtitle = "disconnected topics ==> only connected to filtered out general topics") +
  scale_edge_colour_viridis() +
  geom_node_point(aes(filter = is_relevant == TRUE)) +
  geom_node_label(aes(label = topic, 
                      filter = is_relevant == TRUE),
                  repel = T) +
  theme_graph(title_size = 11) 

# ggsave("figs/enron70_topicCorr.png", width = 12)
