---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Import Data

We get the data from the first notebook.

```{r}
nodes_df <- feather::read_feather("tmp/Enron_vertexlist_no_community.feather")
weigted_edges_df <- feather::read_feather("tmp/Enron_edgelist_no_community.feather")
```


### Building Graph

 - Recall that we only consider emalis of type `TO` between core employees (see `4_Network_EDA.Rmd`)

```{r}
g <- tbl_graph(nodes = nodes_df, edges = weigted_edges_df)
g
```

 - What is the propotions of emails sent within community?

```{r, echo=FALSE}
# TO CHECK - NOT WORKING
# edges_df <- soc_data
# edges_df3 <- edges_df %>%
#   #select(-c(is.sender.core, is.receiver.core)) %>%
#   left_join(nodes_community, by=c("sender_id" = "name")) %>%
#   rename(community_sender = gr_infomap)
# 
# #edges_df3$to <- as.character(edges_df3$to)
# edges_df3$receiver_id <- as.character(edges_df3$receiver_id)
# 
# edges_df3 <- edges_df3 %>%
#   left_join(nodes_community, by=c("receiver_id" = "name")) %>%
#   rename(community_receiver = gr_infomap)
# 
# edges_df3$community_receiver <- as.factor(edges_df3$community_receiver)
# edges_df3$community_sender <- as.factor(edges_df3$community_sender)
# 
# edges_df3$same_community <- ifelse(edges_df3$community_receiver == edges_df3$community_sender, 1, 0)
# 
# # removing self-loops
# edges_df3 <- edges_df3 %>%
#   filter(sender_id != receiver_id)
# 
# #72% stays of emails are sent within community
# (sum(edges_df3$same_community) / nrow(edges_df3) * 100) %>% round(2)
```


## Temporal Graph Analysis
  
 - Why do we need to add temporality?  
    + Because we are interested in a nested dynamical network, i.e. how semantic communities coevolve with social networks. 
      + Semantic communities are link communities, that is, they live on the edges. They are the content in the dynamics of email exchanged between enron employees. 
      + Social network are defined by the structure of the network at the nodes-level. The usual assumption here is that communities share more edges within a them than with other communities. We are interested in how the content of email evolve over time as people adopt social communities. 
    + For simplicity, we will aggreagate at the community level. And look at how the evolution of topics is influenced by the communities.

 - __Issues__: 
    + with our weighted graph, we lost time. Our multi-edges has been collapsed into single ones. 
      + Solution: we take time slices of our network.
    + Because our communities are defined by the fact they share more edges, we have more semantic data within a community than between community. This will influence the correlation between the two systems.
      + Not found yet. to check the literature.

 - What timespan should we use?
   + from Bouveyron et al. 2017: "The considered time window spans from September, 3rd, 2001 to January, 28th, 2002,including three key dates i) September, 11th, 2001: the terrorist attacks to the TwinTowers and the Pentagon (USA). ii) October, 31st, 2001: the Securities and ExchangeCommission (SEC) opened an investigation for fraud concerning Enron. iii) December,2nd, 2001: Enron failed for bankruptcy (more than 4,000 lost jobs). "
 
 - Possible hypothesis:
   + As with ant colonies, this is the rate of interactions that could determine if someone adopt a semantic community. That is, more you talk about it, more you belong to that community. Edges that are "active" (we could use a decay rate function here) will accumulate until we reach some treshold (to learn from data) where you belong to the community. 
 
 -  to Check:
   + Static graph metrics as time series (from https://cran.r-project.org/web/packages/tsna/vignettes/tsna_vignette.html)
 
 - From now on, we focus our attention from `2001-10-03` to `2001-12-01` exclusively. 

################
## UP TO HERE ##
################
 
```{r}
library(lubridate)
edges_df3$date <- ymd(edges_df3$date)
subset_edges_df3 <- edges_df3 %>% 
  arrange(date) %>% 
  filter(date >= "2001-10-03" & date < "2001-12-01") %>% 
  select(date, sender_id, receiver_id, community_sender, community_receiver, core_sender_status, core_receiver_status)
```

 - number of people found in communities

```{r, echo=FALSE}
ID.stack <- c(subset_edges_df3$sender_id, subset_edges_df3$receiver_id)
Community.stack <- c(as.character(subset_edges_df3$community_sender),
                     as.character(subset_edges_df3$community_receiver))
my.t <- table(ID.stack, Community.stack)
v.community <- character(nrow(my.t))
for (i in (1:length(v.community))) {
  v.community[i] <- names(which(my.t[i,]!=0))
}
# number of people found in a community
sort(table(v.community), decreasing = TRUE)
```

- whom is talking to whom?
 
```{r, echo=FALSE}
community.t <- table(subset_edges_df3$community_sender,
                  subset_edges_df3$community_receiver)
community.t <- community.t + t(community.t)
diag(community.t) <- round(diag(community.t)/2)

community.t
```

  - Convert date into duration

```{r, message=FALSE, echo=FALSE}
date_to_days_duration <- function(date) {
  duration <- vector("numeric", length(date))
  for (i in 1:length(date)) {
    duration[i] <- as.duration(date[1] %--% date[i+1])  # operator for interval function in lubridate
  }
  duration <- duration / 86400
  return(duration)  # 86400 seconds in a day
}

subset_edges_df3$time_period <- date_to_days_duration(subset_edges_df3$date)
```

```{r, echo=FALSE}
edges_df3$from <- edges_df3$sender_id
edges_df3$to <- edges_df3$receiver_id

edges_df3 <- edges_df3 %>% 
  arrange(date) %>% 
  filter(date >= "2001-10-03" & date < "2001-12-01")

edges_df3$daily_time_period <- date_to_days_duration(subset_edges_df3$date)
edges_df3$weekly_time_period <- paste0(week(edges_df3[,"date"]), "-", year(edges_df3[,"date"]))

edges_df3 <- edges_df3 %>% 
  drop_na(weekly_time_period)

g <- tbl_graph(nodes = nodes_community, edges = edges_df3) %>% 
  activate(edges) %>% 
  mutate(edge_bet = centrality_edge_betweenness())
```


```{r}
tidy_g
```

```{r, warning=FALSE}
tidy_g %>% 
  activate(edges) %>% 
  ggraph('kk') + 
  geom_edge_fan(aes(colour = same_community, alpha = edge_bet), show.legend = FALSE) +
  geom_node_point(aes(alpha = edge_bet, 
                       col = factor(gr_infomap))) + 
  facet_edges(~weekly_time_period) + 
  theme_graph(foreground = 'steelblue', fg_text_colour = 'white')
  # ggsave("figs/kk_time_community_edge_bet.png") # layout_temporality_community_MeasureCentrality

```


```{r, warning=FALSE}
tidy_g %>% 
  activate(edges) %>% 
  ggraph(layout = 'linear', circular = TRUE) + 
  geom_node_point(aes(alpha = edge_bet, col = factor(gr_infomap))) + 
  geom_edge_arc(aes(colour = same_community, alpha = edge_bet), show.legend = FALSE) + 
  coord_fixed() + 
  facet_edges(~weekly_time_period) +
  theme_graph() +
  theme(legend.justification = "right")
  # ggsave("figs/time_community_edge_bet1.png")

```

------------------------------------------------------

### Calculating all measures every time slice

```{r, echo=FALSE}
  tidy_g %>% 
    as_tibble() %>% 
    filter(message_TO_count == 1) %>% 
    group_by(daily_time_period) %>% 
    summarise(n = n())
```

```{r, echo=FALSE}
create_tidy_g <- function(time_period) {
  edges_new <- tidy_g %>% 
    as_tibble() %>% 
    filter(message_TO_count == 1, 
           weekly_time_period == time_period) %>% 
    select(from, to, 
           sender_id, receiver_id)
  
  nodes_new <- tidy_g %>% 
    activate(nodes) %>% 
    as_tibble() %>% 
    select(name)
  
  # build graph
  g.week <- graph.data.frame(edges_new[,c("sender_id", "receiver_id")], 
                             vertices=nodes_new,
                             directed=FALSE)
  
  # add weight property to edges
  E(g.week)$weight <- 1 
  # collapse multigraph into weighted graph
  wg <- simplify(g.week, edge.attr.comb=list(weight="sum",
                                            sender_id="first",  # the first name will be kept
                                            receiver_id="first"))
  tidy_wg <- as_tbl_graph(wg)
  
  # calculating modularity communiy
  undirected_wg <- as_tbl_graph(as.undirected(wg))
  undirected_tidy_wg <- as_tbl_graph(undirected_g) %>% 
    mutate(fast_greedy = as.factor(group_fast_greedy(weights = weight)))
  
  
  tidy_wg <- tidy_wg %>%
    activate(nodes) %>% 
    mutate(
      # centrality_measures
      deg = centrality_degree(),
      edge_bet = centrality_betweenness(),
      # community algorithms
      gr_infomap = as.factor(group_infomap(weights = weight)),
      gr_walktrap = as.factor(group_walktrap(weights = weight))
  )
  
  return(tidy_wg)
}

create_tidy_edge <- function(time_period) {
    tidy_edge <- create_tidy_g(time_period) %>% 
      activate(edges) %>% 
      as_tibble() %>% 
      mutate(Time = time_period)
    return(tidy_edge)
}

create_tidy_node <- function(time_period) {
    tidy_node <- create_tidy_g(time_period) %>% 
      activate(nodes) %>% 
      as_tibble() %>% 
      mutate(Time = time_period)
    return(tidy_node)
}

create_tidy_edge(list_weeks[1])
create_tidy_node(list_weeks[1])

# binding together edges at each time slice
edges_df_total <- map(list_weeks, create_tidy_edge) %>% 
  bind_rows()

# binding together nodes at each time slice
nodes_df_total <- map(list_weeks, create_tidy_node) %>% 
  bind_rows()

# full g
g_total <- tbl_graph(nodes=nodes_df_total, edges = edges_df_total)

# plotting full g
g_total %>%  
  activate(nodes) %>% 
  filter(deg > 0) %>% 
  ggraph(layout = 'linear', circular = TRUE) + 
  geom_node_point(aes(color = gr_infomap), show.legend = FALSE) +
  geom_edge_arc() + 
  coord_fixed() + 
  facet_edges(~Time) +
  theme_graph() +
  theme(legend.justification = "right")


#################
##### TEST ######
#################

#nodes_df_total %>% 
#  filter(name == "sally.beck@enron.com")
```


```{r, echo=FALSE}
plot_graphs <- function(g) {
  p <- ggraph(g) +
    geom_edge_link(aes(alpha = weight*1.5), show.legend = FALSE) +
    geom_node_point(aes(colour = gr_walktrap,
                        size = deg), show.legend = FALSE) +
    theme_graph()
  
  p2 <- ggraph(tidy_wg) +
    geom_edge_link(aes(alpha = weight*1.5), show.legend = FALSE) +
    geom_node_point(aes(colour = gr_infomap,
                        size = deg), show.legend = FALSE) +
    theme_graph()
  
  p3 <- undirected_tidy_wg %>% 
    mutate(deg = centrality_degree()) %>% 
    ggraph() +
    geom_edge_link(aes(alpha = weight*1.5), show.legend = FALSE) +
    geom_node_point(aes(colour = fast_greedy,
                        size = deg), show.legend = FALSE) +
    theme_graph()
  
  plot_grid(p1, p2, p3,
            labels = c("Walktrap", "Infomap", "Fast Greedy"),
            label_size = 12) + 
    ggtitle(time_period)
}
```


---------------------------------------------------

### Analysis with Network dynamic 
 
```{r, echo=FALSE}
vids <- sort(unique(c(subset_edges_df3$sender_id, subset_edges_df3$receiver_id)))

g.week <- graph.data.frame(subset_edges_df3[, c("sender_id", "receiver_id", "time_period")],
                           vertices = data.frame(vids))

community <- unique(rbind(data.frame(id=subset_edges_df3$sender_id,
                                     community=subset_edges_df3$community_sender),
                          data.frame(id=subset_edges_df3$receiver_id,
                                     community=subset_edges_df3$community_receiver)))

V(g.week)$community <- as.factor(community[order(community[,1]), 2])


status <- unique(rbind(data.frame(id=subset_edges_df3$sender_id,
                                               status=subset_edges_df3$core_sender_status),
                                    data.frame(id=subset_edges_df3$receiver_id,
                                               status=subset_edges_df3$core_receiver_status)))

V(g.week)$status <- as.factor(status[order(status[,1]), 2])

E(g.week)$time_period <- E(g.week)$time_period * 24  # converting days in hours

tidy_week <- as_tbl_graph(g.week)
tidy_week
```
 
```{r, message=FALSE, message=FALSE}
library(networkDynamic)

adding_ID_value <- function() {
  names = unique(c(edges_new$sender_id, 
                   edges_new$receiver_id))
  tmp <- bind_cols(names=names,
                   ID=1:length(names))
  subset_edges <- edges %>% 
    left_join(tmp, by=c("sender_id" = "names")) %>% 
    left_join(tmp, by=c("receiver_id" = "names"))
}


# following Kolaczyk and Csardi 2014, p.186
subset_edges_df3.spls <- cbind(subset_edges_df3$time_period * 24,  # lets say that effects of receiving emails last 2 days
                              (subset_edges_df3$time_period+2) * 24,
                               edges_new$ID.x, edges_new$ID.y)
 
# droping last row because NA
subset_edges_df3.spls <- subset_edges_df3.spls[1:14132,]



dn <- networkDynamic(edge.spells = subset_edges_df3.spls)
```





